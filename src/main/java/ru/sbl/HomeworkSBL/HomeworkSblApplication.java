package ru.sbl.HomeworkSBL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkSblApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeworkSblApplication.class, args);
	}

}
