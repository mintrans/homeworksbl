package ru.sbl.HomeworkSBL.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Логирование контроллеров и сервисов Deposit
 */
@Aspect
public class AopLogger {
    private final Logger logger = Logger.getLogger(AopLogger.class.getName());

    private Handler handler;

    {
        try {
            handler = new FileHandler("%h/MyLog.log");
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
    }

    @Before("controllerLog()")
    public void startControllerLog(JoinPoint jp) {
        logger.info("Begin " + jp.getSignature());
    }

    @Pointcut("within(ru.sbl.HomeworkSBL.controller.DepositController)")
    public void controllerLog() { }

    @After("controllerLog()")
    public void finishControllerLog(JoinPoint jp) {
        logger.info("Finish controller" + jp.getSignature());
    }

    @Before("serviceLog()")
    public void startServiceLog(JoinPoint jp) {
        logger.info("Begin " + jp.getSignature());
    }

    @Pointcut("within(ru.sbl.HomeworkSBL.service.DepositService)")
    public void serviceLog() { }

    @After("serviceLog()")
    public void finishServiceLog(JoinPoint jp) {
        logger.info("Finish " + jp.getSignature());
    }
}
