package ru.sbl.HomeworkSBL.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.sbl.HomeworkSBL.model.Deposit;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Взаимодействие с базой данных с использованием JdbcTemplate.
 */
public class DepositDao {

    private JdbcTemplate jdbcTemplate;

    public DepositDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Добавление объекта Person в базу данных
     * @param deposit - добавляемый объект Deposit
     * @return - true при удачном добавлении
     */
    public void create(Deposit deposit) {
        final String sqlQuery = String.format("INSERT INTO deposits ('balance', 'active') " +
                        " VALUES ('%s', '%b')",
                deposit.getBalance(), deposit.isActive());
        jdbcTemplate.update(sqlQuery);
    }

    /**
     * Получение списка всех объектов Person из базы данных
     * @return - {@link ArrayList} Person
     */
    public List<Deposit> getAll() {
        return jdbcTemplate.query("SELECT * FROM deposits",
                new BeanPropertyRowMapper<>(Deposit.class));
    }

    /**
     * Получение объекта класса Deposit
     * из базы данных по id
     * @param id - идентификатор в базе данных
     * @return - найденный Deposit
     */
    public Deposit getById(long id) {
        return jdbcTemplate.query("SELECT * FROM deposits WHERE id = ?",
                new Object[] { id },
                new BeanPropertyRowMapper<>(Deposit.class))
                .stream().findAny().orElse(null);
    }

    /**
     * Изменений объекта класса Deposit по id
     * @param deposit - новый Deposit
     * @return - true если изменение прошло успешно
     */
    public void updateDeposit(Deposit deposit) {
        final String sqlQuery = String.format("UPDATE deposits SET balance = '%s', active = '%b' WHERE id = '%d';",
                deposit.getBalance(), deposit.isActive(), deposit.getId());
        jdbcTemplate.update(sqlQuery);
    }

    /**
     * Удаление объекта класса Deposit по id
     * @param id - идентификатор в базе данных
     * @return - true если удаление прошло успешно
     */
    public void deleteDeposit(long id) {
        jdbcTemplate.update("DELETE FROM deposits WHERE id = ?", Long.valueOf(id));
    }
}
