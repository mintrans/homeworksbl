package ru.sbl.HomeworkSBL.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.sbl.HomeworkSBL.aop.AopLogger;

@Configuration
public class SpringAopConfig {

    @Bean
    public AopLogger loggerComponent() {
        return new AopLogger();
    }
}
