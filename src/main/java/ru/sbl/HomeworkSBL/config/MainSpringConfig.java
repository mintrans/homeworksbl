package ru.sbl.HomeworkSBL.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.sbl.HomeworkSBL.aop.AopLogger;
import ru.sbl.HomeworkSBL.controller.DepositController;
import ru.sbl.HomeworkSBL.dao.DepositDao;
import ru.sbl.HomeworkSBL.service.DepositService;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Конфигуратор приложения
 */
@Configuration
@EnableTransactionManagement
@ComponentScan("ru.sbl.HomeworkSBL")
@PropertySource(value = {"classpath:db.properties"})
public class MainSpringConfig {

    private static final String DB_DRIVER = "db.driver";
    private static final String DB_URL = "db.url";
    private static final String DB_USER = "db.username";
    private static final String DB_PASSWORD = "db.password";

    @Resource
    private Environment env;

    /**
     * Настройка параметро подключения к базе данных из внешнего файла параметров.
     * @return параметры работы с базой данных
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty(DB_DRIVER));
        dataSource.setUrl(env.getRequiredProperty(DB_URL));
        dataSource.setUsername(env.getRequiredProperty(DB_USER));
        dataSource.setPassword(env.getRequiredProperty(DB_PASSWORD));
        return dataSource;
    }

    /**
     * Подключение к базе данных.
     * @param dataSource параметры подключения к базе данных.
     * @return объект подключения к базе данных.
     */
    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        return jdbcTemplate;
    }

    /**
     * Подключение класса DAO для работы с базой данных.
     * @param jdbcTemplate объект подключения к базе данных.
     * @return Репозиторий взаимодействия с базой данных.
     */
    @Bean
    public DepositDao depositDao(JdbcTemplate jdbcTemplate) {
        return new DepositDao(jdbcTemplate);
    }

    /**
     * Подключение класса Сервиса для работы с Репозиторием.
     * @param depositDao Репозиторий
     * @return Сервис взаимодействия с Репозиторием.
     */
    @Bean
    public DepositService depositService(DepositDao depositDao) {
        return new DepositService(depositDao);
    }

    /**
     * Подключение класса Контроллеров для работы с Сервисом.
     * @param depositService Сервис.
     * @return Котроллеры взаимодествиующие с Сервисом.
     */
    @Bean
    public DepositController depositController(DepositService depositService) {
        return new DepositController(depositService);
    }
}
