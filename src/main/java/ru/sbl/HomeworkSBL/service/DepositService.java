package ru.sbl.HomeworkSBL.service;

import ru.sbl.HomeworkSBL.dao.DepositDao;
import ru.sbl.HomeworkSBL.model.Deposit;

import java.util.List;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Класс-сервис взаимодействия контроллеров с репозиторием.
 */
public class DepositService {

    private DepositDao depositDao;

    public DepositService(DepositDao depositDao) {
        this.depositDao = depositDao;
    }

    /**
     * Получение всех депозитов.
     * @return список депозитов.
     */
    public List<Deposit> getAllDeposits() {
        return depositDao.getAll();
    }

    /**
     * Получение депозита по id.
     * @param id депозита.
     * @return найденный депозит.
     */
    public Deposit getById(Long id) {
        return depositDao.getById(id);
    }

    /**
     * Открытие депозита.
     * @return true - если успешно.
     */
    public void createDeposit() {
        Deposit deposit = new Deposit();
        deposit.setBalance(0);
        deposit.setActive(true);
        depositDao.create(deposit);
    }

    /**
     * Изменение депозита.
     * @param deposit новый Deposit
     * @return true - если успешно.
     */
    public void updateDeposit(Deposit deposit) {
        depositDao.updateDeposit(deposit);
    }

    /**
     * Удаление депозита.
     * @param id удаляемого депозита.
     * @return true - если успешно.
     */
    public void deleteDeposit(Long id) {
        depositDao.deleteDeposit(id);
    }

    /**
     * Деактивация депозита.
     * @param id
     * @return true - если успешно.
     */
    public void setDepositActive(Long id) {
        Deposit deposit = depositDao.getById(id);
        if (deposit.isActive()) deposit.setActive(false);
        else deposit.setActive(true);
        depositDao.updateDeposit(deposit);
    }
}
