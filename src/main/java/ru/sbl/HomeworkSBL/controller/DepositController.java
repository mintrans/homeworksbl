package ru.sbl.HomeworkSBL.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sbl.HomeworkSBL.model.Deposit;
import ru.sbl.HomeworkSBL.service.DepositService;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Контроллеры страниц работы с Deposit
 */
@RequestMapping("/deposit")
public class DepositController {

    private DepositService service;

    public DepositController(DepositService depositService) {
        this.service = depositService;
    }

    /**
     * Открытие нового Deposit в базе данных.
     * @return страницу со всеми Deposit включая новый.
     */
    @GetMapping(value = "/create")
    public ModelAndView createDeposit() {
        service.createDeposit();

        ModelAndView model = new ModelAndView();
        model.addObject("depositsList", service.getAllDeposits());
        model.setViewName("home");
        return model;
    }

    /**
     * Вывод всех депозитов.
     * @return страницу со всеми Deposit.
     */
    @GetMapping(value = "/all")
    public ModelAndView getAllDeposits() {
        ModelAndView model = new ModelAndView();
        model.addObject("depositsList", service.getAllDeposits());
        model.setViewName("home");
        return model;
    }

    /**
     * Удвление Deposite
     * @param id удаляемого Deposit
     * @return страницу со всеми Deposit с изменениями
     */
    @GetMapping(value = "/delete/{id}")
    public ModelAndView deleteDeposit(@PathVariable (value = "id") long id) {
        service.deleteDeposit(id);

        ModelAndView model = new ModelAndView();
        model.addObject("depositsList", service.getAllDeposits());
        model.setViewName("home");
        return model;
    }

    /**
     * Активизация/деактивизация Deposite
     * @param id активируемого/деактивируемого Deposit
     * @return страницу со всеми Deposit с изменениями
     */
    @GetMapping(value = "/active/{id}")
    public ModelAndView activeDeposit(@PathVariable (value = "id") long id) {
        service.setDepositActive(id);

        ModelAndView model = new ModelAndView();
        model.addObject("depositsList", service.getAllDeposits());
        model.setViewName("home");
        return model;
    }

    /**
     * Загрузка страницы с Deposit по id для изменений.
     * @param id загружаемого Deposit
     * @return страницу для редактирования Deposit
     */
    @GetMapping(value = "/update/{id}")
    public ModelAndView updateDeposit(@PathVariable (value = "id") long id) {
        ModelAndView model = new ModelAndView();
        model.addObject("deposit", service.getById(id));
        model.setViewName("dep");
        return model;
    }

    /**
     * Сохранение внесенных изменений в Deposit
     * @param deposit новые данные Deposit
     * @return страницу со всеми Deposit с изменениями
     */
    @PostMapping("/update")
    public ModelAndView saveUpdateDeposit(@ModelAttribute("deposite") Deposit deposit) {
        Deposit depositDB  = service.getById(deposit.getId());
        depositDB.setBalance(deposit.getBalance());
        depositDB.setActive(deposit.isActive());

        service.updateDeposit(depositDB);

        ModelAndView model = new ModelAndView();
        model.addObject("depositsList", service.getAllDeposits());
        model.setViewName("home");
        return model;
    }
}
