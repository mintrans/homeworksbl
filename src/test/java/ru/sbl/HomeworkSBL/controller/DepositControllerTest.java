package ru.sbl.HomeworkSBL.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.sbl.HomeworkSBL.dao.DepositDao;
import ru.sbl.HomeworkSBL.model.Deposit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Тест сервиса работы с базой данных.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class DepositControllerTest {
    Deposit deposit1,deposit2, deposit3;
    DepositDao mockDao;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void start() {
        deposit1 = new Deposit();
        deposit1.setId(1);
        deposit1.setBalance(1000);
        deposit1.setActive(false);

        deposit2 = new Deposit();
        deposit2.setId(2);
        deposit2.setBalance(5000);
        deposit2.setActive(true);

        deposit3 = new Deposit();
        deposit3.setId(3);
        deposit3.setBalance(13000);
        deposit3.setActive(true);

        mockDao = mock(DepositDao.class);
    }

    /**
     * Тестирование коньроллера создание Депозита
     */
    @Test
    public void createDepositControllerTest() throws Exception {
        Deposit deposit = new Deposit();
        mockDao.create(deposit);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.get("/deposit/create")
                .content(this.mapper.writeValueAsString(deposit));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера получения всех записей
     */
    @Test
    public void getAllDepositsControllerTest() throws Exception {
        List<Deposit> list = new ArrayList<>(Arrays.asList(deposit1, deposit2, deposit3));
        Mockito.when(mockDao.getAll()).thenReturn(list);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/deposit/all")
                .contentType(MediaType.ALL))
                .andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера удаления записи по id
     */
    @Test
    public void deleteDepositControllerTest() throws Exception {
        mockDao.deleteDeposit(3);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .get("/deposit/delete/3")
                .contentType(MediaType.ALL)
                .content(this.mapper.writeValueAsString(deposit3));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера активации депозита
     */
    @Test
    public void activeDepositControllerTest() throws Exception {
        Mockito.when(mockDao.getById(2)).thenReturn(deposit2);
        mockDao.updateDeposit(deposit2);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .get("/deposit/active/2")
                .contentType(MediaType.ALL)
                .content(this.mapper.writeValueAsString(deposit2));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера вывода на экран формы редактирования депозита по id
     */
    @Test
    public void updateDepositControllerTest() throws Exception {
        Mockito.when(mockDao.getById(1)).thenReturn(deposit1);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .get("/deposit/update/1")
                .contentType(MediaType.ALL)
                .content(this.mapper.writeValueAsString(deposit1));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }

    /**
     * Тестирование контроллера внесения изменений в депозит
     */
    @Test
    public void saveUpdateDepositControllerTest() throws Exception {
        mockDao.updateDeposit(deposit1);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
                .post("/deposit/update")
                .contentType(MediaType.ALL)
                .content(this.mapper.writeValueAsString(deposit1));

        mockMvc.perform(mockRequest).andExpect(status().isOk());
    }
}
