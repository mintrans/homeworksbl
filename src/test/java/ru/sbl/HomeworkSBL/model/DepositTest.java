package ru.sbl.HomeworkSBL.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

/**
 * @author Nochkin Evgeniy
 * @version 1.0
 * Тестирование сущности {@link Deposit}
 */
public class DepositTest {
    private Deposit deposit;

    @Before
    public void start() {
        deposit = new Deposit();
        deposit.setId(12);
        deposit.setBalance(15150.30);
        deposit.setActive(true);
    }

    /**
     * Тестирование геттеров
     */
    @Test
    public void depositGetterTest() {
        Assertions.assertEquals(12, deposit.getId());
        Assertions.assertEquals(15150.30, deposit.getBalance());
        Assertions.assertEquals(true, deposit.isActive());
    }

    /**
     * Тестирование сеттеров
     */
    @Test
    public void depositSetterTest() {
        deposit.setId(15);
        Assertions.assertEquals(deposit.getId(), 15);
        deposit.setBalance(895000.10);
        Assertions.assertEquals(deposit.getBalance(), 895000.10);
        deposit.setActive(false);
        Assertions.assertEquals(deposit.isActive(), false);
    }
}
